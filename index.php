<?php
define('DIR_APP', __DIR__ . '/src/');

try {
    spl_autoload_register(function ($className) {
        require_once DIR_APP . $className . '.php';
    });
    $route = $_GET ['route'] ?: '/';
    $routes = require DIR_APP . 'routes.php';
    foreach ($routes as $pattern => $controllerAndMethod) {
        preg_match($pattern, $route, $matches);
        if (!empty($matches)) {
            break;
        }
    }
    unset($matches[0]);
    $controllerName = $controllerAndMethod[0];
    $method = $controllerAndMethod[1];
    $controller = new $controllerName();
    $controller->$method(...$matches);
} catch (\exceptions\DbException $exception) {
    echo $exception->getMessage();
    echo $exception->getTraceAsString();
}


