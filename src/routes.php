<?php
return [
  '`^/$`' => ['controllers\MainController', 'actionIndex'],
  '`^/(.*)$`' => ['controllers\MainController', 'actionRedirect'],
];