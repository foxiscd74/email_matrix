<?php

namespace models;

use services\DB;
use services\kernel\Kernel;

/**
 * Class UrlManager
 * Модель таблицы 'url_manager'
 * @author Grigory Alexandrov
 * @package models
 *
 * @property string $longUrl Оригинальный URL
 * @property string $shortUrl Короткий URL
 */
class UrlManager extends ActiveRecord
{
    public $longUrl;
    public $shortUrl;

    /**
     * Правила валидации
     * @return array[]
     */
    public function rules()
    {
        return [
            [['longUrl'], 'required'],
            [['longUrl'], 'string'],
        ];
    }

    /**
     * @return string
     */
    protected static function getTableName(): string
    {
        return 'url_manager';
    }

    /**
     * Поиск по оригинальому URL
     * @param $long_url
     * @return self|null
     */
    public static function getByLongUrl($long_url)
    {
        $db = DB::getInstance();
        $entities = $db->query(
            'SELECT * FROM `' . static::getTableName() . '` WHERE long_url=:long_url;',
            [':long_url' => $long_url],
            static::class
        );
        return $entities ? $entities[0] : null;
    }

    /**
     * Поиск по короткому URL
     * @param $short_url
     * @return mixed|null
     */
    public static function getByShortUrl($short_url)
    {
        $db = DB::getInstance();
        $entities = $db->query(
            'SELECT * FROM `' . static::getTableName() . '` WHERE short_url=:short_url;',
            [':short_url' => $short_url],
            static::class
        );

        return $entities ? $entities[0] : null;
    }

    /**
     * @param $longUrl
     */
    public function setLongUrl($longUrl)
    {
        $this->longUrl = $longUrl;
    }

    /**
     * @param $shortUrl
     */
    public function setShortUrl($shortUrl)
    {
        $this->shortUrl = $shortUrl;
    }

    /**
     * @return string
     */
    public function getLongUrl()
    {
        return $this->longUrl;
    }

    /**
     * @return string
     */
    public function getShortUrl()
    {
        return $this->shortUrl;
    }
}