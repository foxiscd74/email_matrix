<?php

namespace models;


use exceptions\ValidateException;
use services\DB;

/**
 * Class ActiveRecord
 * @package models
 *
 * @property int $id
 */
abstract class ActiveRecord
{
    protected $id;

    /**
     * Возвращает название таблицы
     * @return string
     */
    abstract protected static function getTableName(): string;

    /**
     * Возвращает правила валидации
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Возвращает результат проверки валидности модели
     * @return bool
     */
    public function validate()
    {
        try {
            if ($this->rules()) {
                foreach ($this->rules() as $rule) {
                    switch ($rule[1]) {
                        case 'required':
                            $method = 'isRequired';
                            break;
                        case 'string':
                            $method = 'isString';
                            break;
                        case 'number':
                            $method = 'isNumber';
                            break;
                        case 'safe':
                            $method = 'safe';
                            break;
                    }
                    if (is_array($rule[0])) {
                        foreach ($rule[0] as $rule) {
                            $this->$method($rule);
                        }
                    } else {
                        $this->$method($rule[0]);
                    }
                }
                return true;
            }
        } catch (ValidateException $exception) {
            echo $exception->getMessage();
        }
    }

    /**
     * Проверка значения обязательного поля
     * @param string $propertyStr названия свойства
     * @return bool
     * @throws ValidateException
     */
    private function isRequired($propertyStr)
    {
        if (!empty($this->$propertyStr)) {
            return true;
        } else {
            throw new ValidateException('Поле ' . $propertyStr . ' обязательно для заполнения');
        }
    }

    /**
     * Проверка значения свойства на строку
     * @param string $propertyStr названия свойства
     * @return bool
     * @throws ValidateException
     */
    private function isString($propertyStr)
    {
        if (is_string($this->$propertyStr) && !is_numeric($this->$propertyStr)) {
            return true;
        } else {
            throw new ValidateException('Поле ' . $propertyStr . ' должно быть строкой');
        }
    }

    /**
     * Проверка значения свойства на число
     * @param string $propertyStr названия свойства
     * @return bool
     * @throws ValidateException
     */
    private function isNumber($propertyStr)
    {
        if (is_numeric($this->$propertyStr)) {
            return true;
        } else {
            throw new ValidateException('Поле ' . $propertyStr . ' должно быть числом');
        }
    }

    /**
     * @param string $propertyStr названия свойства
     * @return bool
     */
    private function safe($propertyStr)
    {
        return true;
    }

    /**
     * Загрузка значений для атрибутов модели
     * @param array $request $_POST
     * @return bool
     */
    public function load($request)
    {
        $vars = array_keys((get_class_vars(static::class)));
        foreach ($request as $key => $value) {
            $key = $this->underscoreToCamelCase($key);
            if (in_array($key, $vars)) {
                $this->$key = $value;
            }
        }
        return true;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $name Название свойства
     * @param mixed $value Значение свойства
     */
    public function __set($name, $value)
    {
        $camelCaseName = $this->underscoreToCamelCase($name);
        $this->$camelCaseName = $value;
    }

    /**
     * Метод преобразования under_score в CamelCase
     * @param string $string строка
     * @return string
     */
    private function underscoreToCamelCase(string $string): string
    {
        return lcfirst(str_replace('_', '', ucwords($string, '_')));
    }

    /**
     * Метод преобразования CamelCase в under_score
     * @param string $string
     * @return string
     */
    private function camelCaseToUnderscore($string)
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $string));
    }

    /**
     * Поиск по ID записи
     * @param int $id
     * @return mixed|null
     */
    public static function getById($id)
    {
        $db = DB::getInstance();
        $entities = $db->query(
            'SELECT * FROM `' . static::getTableName() . '` WHERE id=:id;',
            [':id' => $id],
            static::class
        );

        return $entities ? $entities[0] : null;
    }

    /**
     * Метод сохранения модели
     */
    public function save()
    {
        $mappedProperties = $this->mapPropertiesToDbFormat();
        if ($this->id !== null) {
            $this->update($mappedProperties);
        } else {
            $this->insert($mappedProperties);
        }
    }

    /**
     * Метод изменения модели
     * @param array $mappedProperties
     */
    private function update($mappedProperties)
    {
        $column2params = [];
        $params2values = [];
        $index = 1;
        foreach ($mappedProperties as $column => $value) {
            $param = ':param' . $index;
            $column2params[] = $column . ' = ' . $param;
            $params2values[$param] = $value;
            $index++;
        }
        $sql = 'UPDATE `' . static::getTableName() . '` SET ';
        $sql .= implode(', ', $column2params) . ' WHERE id= ' . $this->id;
        $db = DB::getInstance();
        $db->query($sql, $params2values, static::class);
    }

    /**
     * Метод добавления модели
     * @param array $mappedProperties
     */
    private function insert($mappedProperties)
    {
        $filterProperties = array_filter($mappedProperties);
        $columns = [];
        $params = [];
        $index = 1;
        foreach ($filterProperties as $column => $value) {
            $params[] = ':param' . $index;
            $columns[] = $column;
            $params2values[':param' . $index] = $value;
            $index++;
        }
        $sql = 'INSERT INTO `' . static::getTableName() . '` (';
        $sql .= implode(',', $columns) . ') VALUES (' . implode(',', $params) . ');';
        $db = DB::getInstance();
        $db->query($sql, $params2values, static::class);
        $this->id = $db->getLastInseartId();
        $this->refresh();
    }

    /**
     * Метод сопоставления свойств формату базы данных
     * @return array
     */
    private function mapPropertiesToDbFormat()
    {
        $reflector = new \ReflectionObject($this);
        $properties = $reflector->getProperties();
        $propertyName = [];
        foreach ($properties as $property) {
            $propertyName = $property->getName();
            $propertyNameAsUnderscore = $this->camelCaseToUnderscore($propertyName);
            $mappedProperties[$propertyNameAsUnderscore] = $this->$propertyName;
        }
        return $mappedProperties;
    }

    /**
     * Метод обновления значений свойств модели
     */
    private function refresh()
    {
        $objectFromDb = static::getById($this->id);
        $reflector = new \ReflectionObject($objectFromDb);
        $properties = $reflector->getProperties();
        foreach ($properties as $property) {
            $property->setAccessible(true);
            $propertyName = $property->getName();
            $this->$propertyName = $property->getValue($objectFromDb);
        }
    }

    /**
     * Удаление экземпляра модели
     */
    public function delete()
    {
        $db = DB::getInstance();
        $db->query('DELETE FROM `' . static::getTableName() . '` WHERE id = :id', [':id' => $this->id]);
        $this->id = null;
    }
}