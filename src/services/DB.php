<?php

namespace services;

use controllers\AbstractControllers;
use exceptions\DbException;
use PDO;
use PDOException;

/**
 * Class DB makes a connection to the database
 *
 * @author Grigory Alexandrov
 * @package services
 *
 * @property PDO $pdo
 * @property DB $instance
 */
class DB
{

    private $pdo;
    private static $instance;

    /**
     * DB constructor.
     */
    private function __construct()
    {
        $dboptions = (require DIR_APP . 'config/local-config.php')['db'];
        try {
            $this->pdo = new PDO(
                'mysql:host=' . $dboptions['host'] . ';dbname=' . $dboptions['dbname'],
                $dboptions['user'],
                $dboptions['password']
            );
            $this->pdo->exec('SET NAMES UTF8');
        } catch (PDOException $e) {
            throw new DbException('Ошибка при подключении к базе данных' . $e->getMessage());
        }
    }

    /**
     * @param string $sql Строка запроса
     * @param array $params Параметры поиска
     * @param string|string $className
     * @return array|null
     */
    public function query($sql, array $params = [], $className = 'stdClass')
    {
        $sth = $this->pdo->prepare($sql);
        $result = $sth->execute($params);
        if (false === $result) {
            return null;
        }
        return $sth->fetchAll(PDO::FETCH_CLASS, $className);
    }

    /**
     * @return int
     */
    public function getLastInseartId()
    {
        return (int)$this->pdo->lastInsertId();
    }

    /**
     * Шаблон одиночка
     * @return DB
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
}
