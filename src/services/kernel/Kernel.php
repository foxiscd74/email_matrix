<?php

namespace services\kernel;


use exceptions\KernelException;

/**
 * Class Kernel
 * Ядро проекта, с помощью него можно получить доступ до различных сервисов
 * пример вызова:
 *
 * Kernel::getKernel()->getOperations()->testOperation($arguments);
 *
 * Для добавления новых операций необходимо:
 *
 * 1) наследоваться от AbstractKernelOperation
 *
 * 2) добавить компонент в настройки системы (config/local_config.php)
 *
 * @package services\kernel
 *
 * @property string $component
 * @property KernelInterface $instance
 * @property object[] $operations_objects
 */
class Kernel implements KernelInterface
{
    private $instance;
    private $operations_objects = [];
    private $component = '';

    /**
     * @return KernelInterface
     */
    public function getOperations()
    {
        $this->component = 'getOperations';
        return $this->instance;
    }

    /**
     * Возвращает значение алиасов
     * @param string $alias_name Название алиаса
     * @return mixed
     */
    public static function getAlias($alias_name)
    {
        $aliases = (include DIR_APP . 'config/local-config.php')['aliases'];
        foreach ($aliases as $name => $alias) {
            if ($name == $alias_name) {
                return $alias;
            }
        }
    }

    /**
     * Возвращает экземпляр ядра с доступом к внутренним компонентам
     * @return KernelInterface
     */
    public static function getKernel()
    {
        $kernel = new static();
        $kernel->instance = $kernel;
        $operations = (include DIR_APP . 'config/local-config.php')['operations'];
        foreach ($operations as $operation) {
            $kernel->operations_objects[] = new $operation;
        }
        return $kernel->instance;
    }

    /**
     * @param string $name Название вызванного метода
     * @param null $arguments Аргументы вызываемого метода
     * @return mixed
     */
    public function __call($name, $arguments = null)
    {
        try {
            switch ($this->component) {
                case 'getOperations':
                    return $this->getOperationMethod($name, $arguments);
            }
        } catch (KernelException $exception) {
            exit($exception->getMessage());
        }
    }

    /**
     * Возвращает метод операции
     * @param string $name Название вызванного метода
     * @param array|null $arguments Аргументы вызываемого метода
     * @return mixed
     */
    private function getOperationMethod($name, $arguments)
    {
        foreach ($this->operations_objects as $operation) {
            if (method_exists($operation, $name)) {
                $obj = new \ReflectionClass($operation);
                if ($obj->getMethod($name)->isPublic()) {
                    return $operation->$name(...$arguments);
                }
                throw new KernelException('Метод ' . $name . ' должен быть public');
            }
        }
        throw new KernelException('Метода ' . $name . ' не существует');
    }

}