<?php


namespace services\kernel;

/**
 * Interface KernelInterface
 * @package services\kernel
 */
interface KernelInterface
{
    /**
     * Возвращает все доступные операции ядра
     * @return KernelOperationInterface
     */
    public function getOperations();
}