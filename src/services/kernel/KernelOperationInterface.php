<?php

namespace services\kernel;

use models\UrlManager;

/**
 * Interface KernelOperationInterface
 * Интерфейс для отображения phpdoc операций ядра
 * @package services\kernel
 */
interface KernelOperationInterface
{
    /**
     * Вовзвращает короткую ссылку для ресурса
     * @param null $long_url Оригинальная ссылка
     * @return string
     */
    public function getShortUrl($long_url = null);
}