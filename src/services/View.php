<?php

namespace services;

/**
 * Class View
 * @package services
 *
 * @property string $templatesPath Путь к шаблону
 */
class View
{
    private $templatesPath;
    private $layout;
    private $content;

    /**
     * View constructor.
     * @param string $templatesPath Путь к шаблону
     */
    public function __construct($templatesPath)
    {
        $this->templatesPath = $templatesPath;
    }

    /**
     * Загружает страницу
     * @param string $content
     */
    private function loadPage($content)
    {
        ob_start();
        include $this->getLayout();
        $buffer = ob_get_contents();
        ob_end_clean();
        echo $buffer;
    }

    /**
     * Вывод шаблона страницы
     * @param string $templateName Название шаблона
     * @param array $vars Параметры переданные в шаблон
     * @return string
     */
    public function render(string $templateName, array $vars = [])
    {
        extract($vars);
        ob_start();
        include $this->templatesPath . '/' . $templateName . '.php';
        $buffer = ob_get_contents();
        ob_end_clean();
        $this->loadPage($buffer);
    }

    /**
     * Установить шаблон страницы
     * @param $layoutName
     */
    public function setLayout($layoutName)
    {
        $this->layout = DIR_APP . 'views/layouts/' . $layoutName . '.php';
    }

    /**
     * Получить шаблон страницы
     * @return string
     */
    public function getLayout()
    {
        if ($this->layout) {
            return $this->layout;
        }
        return DIR_APP . 'views/layouts/index.php';
    }

    /**
     * Перенаправляет на указанный путь
     * @param string $url Путь
     */
    public function redirect($url)
    {
        header('Location: ' . $url);
    }
}