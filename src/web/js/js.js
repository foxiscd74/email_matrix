//Копирует короткую ссылку
function copyLink() {
    var link = document.getElementById('link');
    var range = document.createRange();
    range.selectNode(link);
    window.getSelection().addRange(range);
    document.execCommand('copy');
    window.getSelection().removeAllRanges();
};

// Обновляет поле для ввода ссылки
function refresh() {
    document.getElementById('refresh').click();
};