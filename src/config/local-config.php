<?php
return [
    //Настройки базы данных
    'db' => require_once DIR_APP . 'config/db_options.php',
    //Список операций проекта
    'operations' => [
        \operations\UrlManagerOperation::class,
    ],
    'aliases' => [
        '@domain' => 'http://testemailmatrix.ru',
    ]
];