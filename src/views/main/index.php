<?php
/**
 * @var string $shortUrl
 */
?>
<?php if ($shortUrl): ?>
    <input id="link" type="text" value="<?= $shortUrl ?>" disabled>
    <button onclick="copyLink()">Copy</button>
    <button onclick="refresh()">Refresh</button>
    <form hidden action="/">
        <button id="refresh"></button>
    </form>
<?php else: ?>
    <form action="/" method="post">
        <input type="text" name="long_url">
        <input type="submit" value="Получить короткую ссылку">
    </form>
<?php endif; ?>


