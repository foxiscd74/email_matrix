<?php

namespace controllers;

use models\UrlManager;
use services\kernel\Kernel;

/**
 * Class MainController
 * @author Grigory Alexandrov
 * @package controllers
 */
class MainController extends AbstractController
{
    /**
     * Главная страница
     */
    public function actionIndex()
    {
        if ($long_url = $_POST['long_url']) {
            if ($model = UrlManager::getByLongUrl($long_url)) {
                $shortUrl = $model->shortUrl;
            } else {
                $model = new UrlManager();
                if ($model->load($_POST) && $model->validate()) {
                    $shortUrl = Kernel::getKernel()->getOperations()->getShortUrl($long_url,'g','d');
                    $model->shortUrl = $shortUrl;
                    $model->save();
                }
            }
            return $this->view->render('index', ['shortUrl' => $shortUrl]);
        }
        return $this->view->render('index');
    }

    /**
     * Редирект по короткой ссылке
     */
    public function actionRedirect()
    {
        $url = 'http://testemailmatrix.ru' . $_SERVER['REQUEST_URI'];
        $model = UrlManager::getByShortUrl($url);
        if ($model) {
            $this->view->redirect($model->longUrl);
        } else {
            $this->view->redirect('/');
        }
    }
}