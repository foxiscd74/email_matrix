<?php
namespace controllers;

use services\View;

/**
 * Class AbstractController
 * @package controllers
 *
 * @property View $view экземпляр View
 */
abstract class AbstractController
{
    protected $view;

    /**
     * AbstractController constructor.
     */
    public function __construct()
    {
        $controllerName = $this->parseControllerName();
        $this->view = new View(DIR_APP . 'views/' . $controllerName);
    }

    /**
     * Парсит название контроллера
     * @return string
     */
    private function parseControllerName()
    {
        preg_match('`\\\(.*)$`', static::class, $matches);
        return strtolower(str_ireplace('Controller', '', $matches[1]));
    }
}