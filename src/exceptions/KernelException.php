<?php
namespace exceptions;

/**
 * Class KernelException
 * @author Grigory Alexandrov
 * @package exceptions
 */
class KernelException extends \Exception
{
}