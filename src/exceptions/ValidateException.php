<?php
namespace exceptions;

/**
 * Class ValidateException
 * @author Grigory Alexandrov
 * @package exceptions
 */
class ValidateException extends \Exception
{
}