<?php
namespace exceptions;

/**
 * Class DbException
 * @author Grigory Alexandrov
 * @package exceptions
 */
class DbException extends \Exception
{
}