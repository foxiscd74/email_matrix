<?php

namespace operations;

use models\UrlManager;
use services\kernel\AbstractKernelOperation;
use services\kernel\Kernel;

/**
 * Class UrlManagerOperation
 * Операция для изменения URL с оригинального на короткий
 * @author Grigory Alexandrov
 * @package operations
 */
class UrlManagerOperation extends AbstractKernelOperation
{
    /**
     * Вовзвращает короткую ссылку для ресурса
     * @param null $long_url Оригинальная ссылка
     * @return string
     */
    public function getShortUrl($long_url = null, $r = false, $g = 3)
    {
        preg_match('`(.*):/(.*)/(.*)`', $long_url, $matches);
        $encodeStr = substr(md5($matches[3]), 2, 8);
        $checkedStr = $this->checkStr($encodeStr);
        $url = Kernel::getAlias('@domain') . '/' . $checkedStr;
        return $url;
    }

    /**
     * Проверяет уникальность выдаваемого URL
     * @param string $url урл для проверки в базе
     * @return mixed
     */
    private function checkStr($url)
    {
        $checkModel = UrlManager::getByShortUrl($url);
        if ($checkModel) {
            $url .= rand(0, 9) . $url;
            $url = $this->checkStr($url);
        }
        return $url;
    }
}