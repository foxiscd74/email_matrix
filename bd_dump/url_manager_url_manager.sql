create table url_manager
(
    id        int auto_increment comment 'Идентификатор',
    long_url  text not null comment 'URL ресурса',
    short_url text not null comment 'короткий URL',
    constraint url_manager_id_uindex
        unique (id)
)
    comment 'Таблица коротких ссылок на ресурс';

alter table url_manager
    add primary key (id);

